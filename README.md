## badges

<img alt="Arduino" src="https://img.shields.io/badge/-Arduino-00979D?style=for-the-badge&logo=Arduino&logoColor=white"/> <img alt="Python" src="https://img.shields.io/badge/python-%2314354C.svg?&style=for-the-badge&logo=python&logoColor=white"/> <img alt="C++" src="https://img.shields.io/badge/c++-%2300599C.svg?&style=for-the-badge&logo=c%2B%2B&ogoColor=white"/> <img alt="C" src="https://img.shields.io/badge/c-%2300599C.svg?&style=for-the-badge&logo=c&logoColor=white"/>

## arduiNO

arduino code. I also try other stuff here as well.

## Installation

Don't clone. I experiment here.

## Authors

- [@hakancangunerli](https://www.github.com/hakancangunerli)

## Acknowledgements

Note: this is not 100% my code, I took a lot of advice from all over the place.
